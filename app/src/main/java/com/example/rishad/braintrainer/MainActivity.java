package com.example.rishad.braintrainer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonGo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonGo = (Button) findViewById(R.id.buttonGoId);
        buttonGo.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v.getId()== R.id.buttonGoId){

            buttonGo.setVisibility(v.INVISIBLE);
            Intent intent = new Intent(this,Math.class);
            startActivity(intent);
        }

    }
}
